run:  
ifeq ($(ins),)
	@echo "Usage :"
	@echo "	make ins='6'	# pour l'instance 6"
	@echo "	make ins='all'	# pour toutes les instances"
	@echo "	make ins='6p'	# 'p' pour solution partielle"
else
	@- python3 main.py $(ins)
endif

graphe:
	@- python3 drawGraphe.py
clean:
	@- rm -rf "resultats/"
	@- mkdir "resultats/"
	@- mkdir "resultats/matrices"
	@echo "OK DELETED"
help:
	@echo "Usage :"
	@echo "	make ins='6'	# pour l'instance 6"
	@echo "	make ins='all'	# pour toutes les instances"
	@echo "	make ins='6p'	# 'p' pour solution partielle"

