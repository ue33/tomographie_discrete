import numpy as np
import sys
import glob
import time
import os
import colorama
colorama.init()


class col:
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	
coloriableDic = {
	-1: col.FAIL+"NON"+col.ENDC,
  	0: col.WARNING+"ON NE SAIT PAS"+col.ENDC,
  	1: col.OKGREEN+"OUI"+col.ENDC,
}

"""
	(Q1)
	fonction qui prend en entree j le nombre de case a colorier
	et la sequence associee et renvoit la possibilite de coloriage (bool)
	hypotese : toutes les cases sont vide (non coloree)
	complexite cas de base = O(1) 
	complexite sous problemes = T(0) 
	nombre d'appels = entre l et M 
	# => COMPLEXITE O(M + l)
"""
def testerLigneNonColoree (j, sequence) :
	l = len(sequence)
	if l == 0:  
		return True  # O(1)
	if l >= 1: 
		if j < sequence[-1] - 1 :	#sequence[-1] = Sl
			return False # O(1)
		elif j == sequence[-1] -1 :	
			return l == 1  # O(1)
		else :
			# 	   		l appels au pire cas									    M appels au pire cas
			return testerLigneNonColoree( j - sequence[-1] - 1, sequence[:-1]) or testerLigneNonColoree(j-1, sequence)

"""
	Code de couleur pour les cases
"""
VIDE = -1
BLANCHE = 0
NOIRE = 1

"""
	(Q7)
	fonction qui prend en entree une ligne/colonne, j le nombre de case a colorier
	et la sequence associee et renvoit la possibilite de coloriage (bool)
	hypotese : cases eventuellement coloriee
	complexite cas de base = O(M) 
	complexite sous problemes = entre T(1) et T(M)
	nombre d'appels = entre l et M + l  = M + l
    # => COMPLEXITE O(M^3 + M^2)
"""
def possibiliteColoriage(j, sequence, ligne, resultatsPrecedents):
	l = len(sequence)
	#optimisation
	if resultatsPrecedents[j][l] != -1:
		return resultatsPrecedents[j][l]
	if l == 0:
		# avec une sequence vide on a la seule coloriation possible c'est de colorier toutes
		# les cases en blanches il ne faut pas qu'il y ait une case noire
		possibilite = all([ligne[k] != NOIRE for k in range(j+1)])  # O(M)
	elif l >= 1:
		if j < sequence[-1] - 1:
			possibilite = False  # O(1)
		elif j == sequence[-1] - 1:
			# si le nombre de cases a couvrir est egale au dernier bloc
			# on a possibilite de coloriage si l'on dispose d'un unique bloc
			# ET que il n'y a pas de case blache dans les j+1 premiere cases
			possibilite = (l == 1 and all([ligne[k] != BLANCHE for k in range(j+1)]))  # O(M)
		else:
			# on voit toujours la derniere case, et selon sa couleur on fait un
			#  appelle aux fonctions caseIJblanche() et caseIJnoire() imbriquees definies ci dessous
			def caseIJblanche(): # T(1) et M appels au pire cas
				# on regarde la possibilite de coloriage pour les  j-1 cases restantes
				return possibiliteColoriage(j-1, sequence, ligne, resultatsPrecedents)
			def caseIJnoire():  # T(M) et l appels au pire cas
				# il faut qu'il n y a pas de case blanche pour une longuer de Sl a partir
				# de la fin, et que la case de separation ne soit pas noire, une fois les
				# conditions satisfait, on test la  possibilite de coloriage pour le reste des cases
				conditions = all([ligne[k] != BLANCHE for k in range(j + 1 - sequence[-1], j)]) and ligne[j - sequence[-1]] != NOIRE
				return conditions and possibiliteColoriage(j-sequence[-1]-1, sequence[:-1], ligne, resultatsPrecedents)
			#optimisation
			if sum(sequence)+l-1 > j+1: 
				possibilite = False
			elif ligne[j] == BLANCHE:
				possibilite = caseIJblanche()
			elif ligne[j] == NOIRE:
				possibilite = caseIJnoire()
			elif ligne[j] == VIDE:
				possibilite = caseIJblanche() or caseIJnoire()
			else:
				possibilite = False
	resultatsPrecedents[j][l] = possibilite
	return resultatsPrecedents[j][l]



"""
	(Q9)
	à partir d un fichier contenant les sequence par lignes/colonnes
	on creer la matrice initialement vide et on fait appelle a la fonction enumeration
	complexite cas de base = O(N + M) 
	complexite sous problemes = O(coloration)
	nombre d'appels = 1 
    # => COMPLEXITE O(???)
"""
def propagationPartielle(nomFichier):
    file = open(nomFichier)
    first = True
    sequencesLignes = []
    sequencesColonnes = []
    for line in file:
        sequence = line.strip().split()
        if first:
            if sequence and sequence[0] == "#":
                first = False
            else:
                sequence = [int(el) for el in sequence]
                sequencesLignes.append(sequence)
        else:
            sequence = [int(el) for el in sequence]
            sequencesColonnes.append(sequence)
    grille = -1 * \
    	np.ones((len(sequencesLignes), len(sequencesColonnes)), dtype=int)
    return coloration(grille, sequencesLignes, sequencesColonnes)


"""
	fonction qui prend en arguments la grille et les sequences lignes/colonnes
	et qui fait appel a la function colorerLigCol 

	-1 impossible de colorier
	 0 on ne sait pas 
	 1 matrice coloriee avec success

	retourne OUI_ou_NON_laMAtriceEteColoree, la Matrice resultante
"""
"""
	Code de couleur pour les cases
"""
IMPOSSIBLE = -1
DONT_KNOW = 0
SUCESS = 1

"""
	(Q9)
	pernd en arguments une grille et ses sequences lignes/colonnes
	complexite cas de base = O(1) 
	complexite sous problemes = O(colorerLigCol) = ?????????
	nombre d'appels de sous probleme = N + M
    # => COMPLEXITE (N + M)*O(colorerLigCol) = ?????????
"""
def coloration(grille, sequencesLignes, sequencesColonnes):
	grilleColoree = np.copy(grille)
	N, M = grilleColoree.shape[0],  grilleColoree.shape[1]
	lignesAVoir = set(range(N))
	colonnesAVoir = set(range(M))
	while len(lignesAVoir) != 0 or len(colonnesAVoir) != 0:
		for i in lignesAVoir:
			ok, grilleColoree, nouveaux = colorerLigCol(grilleColoree, i, sequencesLignes)
			if not ok:
				return IMPOSSIBLE, None
			colonnesAVoir = colonnesAVoir.union(nouveaux)
			lignesAVoir = lignesAVoir.difference(set([i]))
		for j in colonnesAVoir:
			ok, transpose, nouveaux = colorerLigCol(grilleColoree.T, j, sequencesColonnes)
			grilleColoree = transpose.T
			if not ok:
				return IMPOSSIBLE, None
			lignesAVoir = lignesAVoir.union(nouveaux)
			colonnesAVoir = colonnesAVoir.difference(set([j]))
	if np.all(grilleColoree != VIDE):
		return SUCESS, grilleColoree
	return DONT_KNOW, grilleColoree


"""
	(Q9)
	fais le coloriage d'une seule ligne ou une seule colonne
	complexite cas de base = O(1)
	complexite sous problemes = O(possibiliteColoriage) = ?????????
	nombre d'appels de sous probleme = 2k avec k=max(M,N) (considerant 2N)
    # => COMPLEXITE 2N*O(possibiliteColoriage) = ?????????
"""
def colorerLigCol (grille, i, sequences) :
	length = len(grille[i])
	nvColonnes = set()
	ok = True
	for j in range(length) : 
		if grille[i][j] == VIDE :
			g = np.copy(grille)
			g[i][j] = NOIRE
			resultatsIntermediares = -1 * np.ones((length, len(sequences[i])+1))
			v1 = possibiliteColoriage(length-1, sequences[i], g[i], resultatsIntermediares)
			g[i][j] = BLANCHE
			resultatsIntermediares = -1 * np.ones((length, len(sequences[i])+1))
			v2 = possibiliteColoriage(length-1, sequences[i], g[i], resultatsIntermediares)
			if v1 and not v2 : 
				nvColonnes.add(j)
				grille[i][j] = NOIRE
				ok = True
			elif not v1 and v2:
				nvColonnes.add(j)
				grille[i][j] = BLANCHE
				ok = True
			elif not v1 and not v2 :
				return False, None, None
	return ok, grille, nvColonnes



"""
	(Q7)
	à partir d un fichier contenant les sequence par lignes/colonnes
	on creer la matrice initialement vide et on fait appelle a la fonction enumeration
	complexite cas de base = O(N + M) 
	complexite sous problemes = O(enumeration)
	nombre d'appels = 1 
    # => COMPLEXITE O(???)
"""
def propagationComplete (nomFichier):
    file = open(nomFichier)
    first = True
    sequencesLignes = []
    sequencesColonnes  = []
    for line in file:
        sequence =  line.strip().split()
        if first : 
            if sequence and sequence[0] == "#":
                first = False
            else:
                sequence = [int(el) for el in sequence]
                sequencesLignes.append(sequence)
        else:
            sequence = [int(el) for el in sequence]
            sequencesColonnes.append(sequence)
    grille = -1 * np.ones((len(sequencesLignes), len(sequencesColonnes)), dtype=int)
    return enumeration (grille, sequencesLignes, sequencesColonnes)



def enumeration(grille, sequencesLignes, sequencesColonnes):
	grillee = np.copy(grille)
	coloriable, grilleResultante = coloration(grillee, sequencesLignes, sequencesColonnes)
	if coloriable == -1:
		return coloriable, None
	if coloriable == 1:
		return coloriable, grilleResultante
	coloriable, resultat = enum_rec(grilleResultante, sequencesLignes, sequencesColonnes, 0, 0)
	if coloriable == 1:
		return coloriable, resultat
	coloriable, resultat = enum_rec(grilleResultante, sequencesLignes, sequencesColonnes, 0, 1)
	if coloriable == 1:
		return coloriable, resultat
	return -1, None


def enum_rec(grille, sequencesLignes, sequencesColonnes, k, couleur):
	grillee = np.copy(grille)
	N, M = grillee.shape[0],  grillee.shape[1]
	if k == M*N:
		return 1, grillee
	i, j = k // M, k % M
	ok, resultat = colorierEtPropager(
		grillee, sequencesLignes, sequencesColonnes, i, j, couleur)
	if ok == -1:
		return -1, None
	if ok == 1:
		return 1, resultat
	stop = False
	for ii in range(N):
		if stop:
			break
		for jj in range(M):
			if resultat[ii][jj] == -1:
				kk = ii*M+jj
				stop = True
				break
	coloriable, grilleResultante = enum_rec(
		resultat, sequencesLignes, sequencesColonnes, kk, 0)
	if coloriable == 1:
		return coloriable, grilleResultante
	coloriable, grilleResultante = enum_rec(
		resultat, sequencesLignes, sequencesColonnes, kk, 1)
	if coloriable == 1:
		return coloriable, grilleResultante

	return -1, None




def colorierEtPropager  (grille, sequencesLignes, sequencesColonnes, i, j, couleur):
	grilleColoree = np.copy(grille)
	if i==j==0 and couleur == grille[i][j]: #cas du premier appel avec i et j = 0 alors que la case est coloré déjà
		return 0 , grilleColoree
	elif i==j==0 and couleur != grille[i][j] and grille[i][j] != -1: 
		return -1, None
	grilleColoree[i][j] = couleur
	#N , M = grilleColoree.shape[0],  grilleColoree.shape[1]
	lignesAVoir = set([i])
	colonnesAVoir = set([j])
	while len(lignesAVoir) != 0 or len(colonnesAVoir) != 0 :
		for ii in lignesAVoir :
			ok, grilleColoree, nouveaux = colorerLigCol(grilleColoree, ii, sequencesLignes)
			if not ok:
				return -1, None
			colonnesAVoir = colonnesAVoir.union(nouveaux)
			lignesAVoir = lignesAVoir.difference(set([ii]))

		for jj in colonnesAVoir :
			ok, transpose, nouveaux = colorerLigCol(grilleColoree.T, jj, sequencesColonnes)
			if not ok:
				return -1, None
			grilleColoree = transpose.T
			lignesAVoir = lignesAVoir.union(nouveaux)
			colonnesAVoir = colonnesAVoir.difference(set([jj]))
	if np.all(grilleColoree != -1) :
		# print(grilleColoree)
		return 1, grilleColoree
	# print(grilleColoree) 
	return 0, grilleColoree



"""
	Pour permettre de tester les deux methodes
	methode = 1 (resolution partielle)
	methode = 2 (resolution complete) -- PAR DEFAUT
"""
methode = 1

def main(argv):
	times = open('resultats/times.txt', 'w')
	# instances = glob.glob('instances/*.txt')
	durations = []
	if not argv or argv[0] == 'all' :
		a = 0
		b = 17
	else:
		a = int(argv[0])
		b = a + 1
	for k in range(a,b):
		print("-----------------------------")
		instance = 'instances/'+str(k)+'.txt'
		start_time = time.time()
		if	methode == 2 : 
			coloriable, resultat = propagationComplete(instance)
		else : 
			coloriable, resultat = propagationPartielle(instance)
		duration = time.time() - start_time
		# print(resultat)
		print(col.WARNING+instance+" "+str(resultat.shape)+col.ENDC)
		print("Coloriable ? : {}".format(coloriableDic[coloriable]))
		if coloriable == 1:
			print('Output : resultats/'+str(k)+'.txt')
			f1 = open('resultats/matrices/matrice_'+str(k)+'.txt', 'w')
			np.savetxt(f1, resultat, fmt='%d')
			f1.close()
			f2 = open('resultats/fig_'+str(k)+'.txt', 'w', encoding="utf-8")
			tmp = np.where(resultat==0, ' ', resultat)
			np.savetxt(f2, np.where(tmp=='1', '■', tmp), fmt='%s', encoding = "utf-8")
			f2.close()
		print("TIME = %.3f seconds" % duration)
		durations.append(duration)
		# times.write(str(resultat.shape[0]*resultat.shape[1])+"\t%.3f"%duration+"\n")
		times.write(str(k)+"\t%.3f"%duration+"\n")
	times.close()
	print("Durée d'execution totale de l'algorithme sur toutes les instances est : %.3f"%sum(durations)+" secondes \n")

if __name__ == "__main__":
   main(sys.argv[1:])
