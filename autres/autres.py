
"""
	(Q7)
	fonction qui prend en entree une ligne/colonne, j le nombre de case a colorier
	et la sequence associee et renvoit la possibilite de coloriage (bool)
	hypotese : cases eventuellement coloriee
	complexite O(M) avec entre l et M appels au max donc
	# COMPLEXITE O(M^2)
"""
def testerLignePartiellementColoreeOLD(j, sequence, ligne):
	"""
	** NB: cette function n'est pas utilisee dans la suite
	elle est remplacer par testerLignePartiellementColoreeOptimise **
	"""
	l = len(sequence)
	if l == 0:
		# avec une sequence vide on a la seule coloriation possible c'est de colorier toutes 
		# les cases en blanches il ne faut pas qu'il y ait une case noire
		return all([ligne[k] != NOIRE for k in range(j+1)]) # O(M)
	if l >= 1:
		if j < sequence[-1] - 1:
			return False # O(1)
		elif j == sequence[-1] - 1:
			# si le nombre de cases a couvrir est egale au dernier bloc
			# on a possibilite de coloriage si l'on dispose d'un unique bloc
			# ET que il n'y a pas de case blache dans les j+1 premiere cases 
			return l == 1 and all([ligne[k] != BLANCHE for k in range(j+1)])  # O(M)
		else:
			def caseIJblanche():  # M appels au pire cas
				# on regarde la possibilite de coloriage dans les  j-1 cases restantes
				return testerLignePartiellementColoreeOLD(j-1, sequence, ligne)
			def caseIJnoire():  # T(M) et l appels au pire cas
				# il faut qu'il n y a pas de case blanche pour une longuer de Sl a partir
				# de la fin, et que la case de separation ne soit pas noire, une fois les
				# conditions satisfait, on test la  possibilite de coloriage pour le reste des cases
				conditions = all([ligne[k] != BLANCHE for k in range( j + 1 - sequence[-1], j)]) and ligne[j - sequence[-1]] != NOIRE
				return conditions and testerLignePartiellementColoreeOLD(j-sequence[-1]-1, sequence[:-1], ligne)
			# on voit toujours la derniere case, et selon sa couleur on fait un
			#  appelle aux fonctions caseIJblanche() et caseIJnoire() imbriquees definies ci dessous
			if ligne[j] == BLANCHE :
				return caseIJblanche()
			elif ligne[j] == NOIRE :
				return caseIJnoire()
			elif ligne[j] == VIDE:
				return caseIJblanche() or caseIJnoire()
			else:
				return False
	