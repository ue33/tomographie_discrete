**Utilisation:**

* -- Executer le programme pour une instance precise, numero 6 par exemple:
  `make ins='6'`
* -- Executer le programme pour toutes les instances:
  `make ins='all'`
* -- Tracer le graphe de temps:
  `make graphe`
* -- Effacer les anciens resultats
  `make clean`
* -- Afficher help:
  `make`
