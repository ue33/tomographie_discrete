from sol_partielle import *

"""
	à partir d'un fichier contenant les sequence lignes/colonnes
	on cree une matrice initialement vide et on fait appel à la fonction enumeration
	
"""
def propagationComplete (nomFichier):
	try:
		file = open(nomFichier)
	except :
		print(nomFichier+col.FAIL+col.UNDERLINE+" FILE NOT FOUND"+col.ENDC)
		exit()
	first = True
	sequencesLignes = []
	sequencesColonnes  = []
	for line in file:
		sequence =  line.strip().split()
		if first : 
			if sequence and sequence[0] == "#":
				first = False
			else:
				sequence = [int(el) for el in sequence]
				sequencesLignes.append(sequence)
		else:
			sequence = [int(el) for el in sequence]
			sequencesColonnes.append(sequence)
	grille = -1 * np.ones((len(sequencesLignes), len(sequencesColonnes)), dtype=int)
	return enumeration (grille, sequencesLignes, sequencesColonnes)


def enumeration(grille, sequencesLignes, sequencesColonnes):
	grillee = np.copy(grille)
	coloriable, grilleResultante = coloration(grillee, sequencesLignes, sequencesColonnes)
	if coloriable == -1:
		return coloriable, None
	if coloriable == 1:
		return coloriable, grilleResultante
	coloriable, resultat = enum_rec(grilleResultante, sequencesLignes, sequencesColonnes, 0, 0)
	if coloriable == 1:
		return coloriable, resultat
	coloriable, resultat = enum_rec(grilleResultante, sequencesLignes, sequencesColonnes, 0, 1)
	if coloriable == 1:
		return coloriable, resultat
	return -1, None


def enum_rec(grille, sequencesLignes, sequencesColonnes, k, couleur):
	grillee = np.copy(grille)
	N, M = grillee.shape[0],  grillee.shape[1]
	if k == M*N:
		return 1, grillee
	i, j = k // M, k % M
	ok, resultat = colorierEtPropager(
		grillee, sequencesLignes, sequencesColonnes, i, j, couleur)
	if ok == -1:
		return -1, None
	if ok == 1:
		return 1, resultat
	stop = False
	for ii in range(N):
		if stop:
			break
		for jj in range(M):
			if resultat[ii][jj] == -1:
				kk = ii*M+jj
				stop = True
				break
	coloriable, grilleResultante = enum_rec(
		resultat, sequencesLignes, sequencesColonnes, kk, 0)
	if coloriable == 1:
		return coloriable, grilleResultante
	coloriable, grilleResultante = enum_rec(
		resultat, sequencesLignes, sequencesColonnes, kk, 1)
	if coloriable == 1:
		return coloriable, grilleResultante

	return -1, None


def colorierEtPropager  (grille, sequencesLignes, sequencesColonnes, i, j, couleur):
	grilleColoree = np.copy(grille)
	if i==j==0 and couleur == grille[i][j]: #cas du premier appel avec i et j = 0 alors que la case est coloré déjà
		return 0 , grilleColoree
	elif i==j==0 and couleur != grille[i][j] and grille[i][j] != -1: 
		return -1, None
	grilleColoree[i][j] = couleur
	lignesAVoir = set([i])
	colonnesAVoir = set([j])
	while len(lignesAVoir) != 0 or len(colonnesAVoir) != 0 :
		for ii in lignesAVoir :
			ok, grilleColoree, nouveaux = colorerLigCol(grilleColoree, ii, sequencesLignes)
			if not ok:
				return -1, None
			colonnesAVoir = colonnesAVoir.union(nouveaux)
			lignesAVoir = lignesAVoir.difference(set([ii]))

		for jj in colonnesAVoir :
			ok, transpose, nouveaux = colorerLigCol(grilleColoree.T, jj, sequencesColonnes)
			if not ok:
				return -1, None
			grilleColoree = transpose.T
			lignesAVoir = lignesAVoir.union(nouveaux)
			colonnesAVoir = colonnesAVoir.difference(set([jj]))
	if np.all(grilleColoree != -1) :
		return 1, grilleColoree
	return 0, grilleColoree

