import matplotlib.pyplot as plt
import numpy  as np


data = np.loadtxt('resultats/times.txt')


x = data[:, 0]
y = data[:, 1]

plt.plot(x, y)
plt.xlabel('Num instance')
plt.ylabel('time (s)')
plt.savefig('resultats/time.png')
print("Graphe dans resultats/")
# plt.show()