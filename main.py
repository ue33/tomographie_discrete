from sol_complete import * 
import sys
import time



coloriableDic = {
	-1: col.FAIL+col.UNDERLINE+"NON"+col.ENDC,
	0: col.WARNING+col.UNDERLINE+"ON NE SAIT PAS"+col.ENDC,
	1: col.OKGREEN+col.UNDERLINE+"OUI"+col.ENDC,
}

"""
	Pour permettre de tester les deux methodes
	methode = 1 (resolution partielle)
	methode = 2 (resolution complete) -- PAR DEFAUT
"""
methode = 2
	
def main(argv):
	times = open('resultats/times.txt', 'w')
	durations = []
	solType = 'Solution Complete'
	if 'p' in argv[0]:
		global methode
		methode = 1
		solType = 'Solution Partielle'
		argv[0] = argv[0].replace('p', '')
	if argv[0] == 'all':
		a = 0
		b = 17
	else:
		a = int(argv[0])
		b = a + 1
	print(col.BOLD+solType+col.ENDC)
	print('Output dans: "resultats/"')
	for k in range(a,b):
		print("-----------------------------")
		instance = 'instances/'+str(k)+'.txt'
		start_time = time.time()
		if	methode == 2 : 
			coloriable, resultat = propagationComplete(instance)
		else : 
			coloriable, resultat = propagationPartielle(instance)
		duration = time.time() - start_time
		print(col.WARNING+instance+" "+str(resultat.shape)+col.ENDC)
		print("Coloriable ? : {}".format(coloriableDic[coloriable]))
		if coloriable == SUCESS:
			f1 = open('resultats/matrices/matrice_'+str(k)+'.txt', 'w')
			np.savetxt(f1, resultat, fmt='%d')
			f1.close()
			f2 = open('resultats/fig_'+str(k)+'.txt', 'w', encoding="utf-8")
			tmp = np.where(resultat==0, ' ', resultat)
			np.savetxt(f2, np.where(tmp=='1', '■', tmp), fmt='%s', encoding = "utf-8")
			f2.close()
		if coloriable == DONT_KNOW:
			f1 = open('resultats/matrices/matrice_'+str(k)+'.txt', 'w')
			np.savetxt(f1, resultat, fmt='%d')
			f1.close()
			f2 = open('resultats/fig_partielle_'+str(k)+'.txt', 'w', encoding="utf-8")
			tmp = np.where(resultat==0, ' ', resultat)
			tmp = np.where(tmp=='-1', '?', tmp)
			np.savetxt(f2, np.where(tmp=='1', '■', tmp), fmt='%s', encoding = "utf-8")
			f2.close()
		print("Temps = %.3f seconds" % duration)
		durations.append(duration)
		times.write(str(k)+"\t%.3f"%duration+"\n")
	print("Temps d'execution totale est : = %.3f seconds" % sum(durations))
	times.close()

if __name__ == "__main__":
   main(sys.argv[1:])
