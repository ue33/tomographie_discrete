import numpy as np


class col:
	OKGREEN = '\033[92m'
	UNDERLINE = '\033[4m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	BOLD = '\033[1m'
	ENDC = '\033[0m'
"""
	Code de couleur pour les cases
"""
VIDE = -1
BLANCHE = 0
NOIRE = 1

"""
	Code de couleur pour les cases
"""
IMPOSSIBLE = -1
DONT_KNOW = 0
SUCESS = 1


"""
	La fonction qui prend en entree j le nombre de case a colorier
	et la sequence associee et renvoit la possibilite de coloriage (bool)
	hypothese : toutes les cases sont vide (non coloree)
"""
def testerLigneNonColoree (j, sequence) :
	l = len(sequence)
	if l == 0:  
		return True  # O(1)
	if l >= 1: 
		if j < sequence[-1] - 1 :	#sequence[-1] = Sl
			return False # O(1)
		elif j == sequence[-1] -1 :	
			return l == 1  # O(1)
		else :
			return testerLigneNonColoree( j - sequence[-1] - 1, sequence[:-1]) or testerLigneNonColoree(j-1, sequence)

"""
	fonction qui prend en entree une ligne/colonne, j le nombre de case a colorier
	et la sequence associee et renvoit la possibilite de coloriage (bool)
	hypotese : cases eventuellement coloriee
	arguments :
		- j : le j de l'enonce
		- sequence : la liste des séquences soit des lignes ou des colonnes 
		- ligne : la ligne de la grille
		- resultatsPrecedents : le tableau des T(j, l) déjà calculés
	
"""
def possibiliteColoriage(j, sequence, ligne, resultatsPrecedents):
	l = len(sequence)
	#optimisation
	if resultatsPrecedents[j][l] != -1:
		return resultatsPrecedents[j][l]
	if l == 0:
		# avec une sequence vide on a la seule coloriation possible c'est de colorier toutes
		# les cases en blanc, il ne faut pas qu'il y ait une case noire
		possibilite = all([ligne[k] != NOIRE for k in range(j+1)])  # O(M)
	elif l >= 1:
		if j < sequence[-1] - 1:
			possibilite = False  # O(1)
		elif j == sequence[-1] - 1:
			# si le nombre de cases à couvrir est egale au dernier bloc
			# on a possibilite de coloriage si l'on dispose d'un unique bloc
			# ET que il n'y a pas de case blache dans les j+1 premiere cases
			possibilite = (l == 1 and all([ligne[k] != BLANCHE for k in range(j+1)]))  # O(M)
		else:
			# on voit toujours la derniere case, et selon sa couleur on fait un
			#  appel aux fonctions caseIJblanche() et caseIJnoire() imbriquees definies ci dessous
			def caseIJblanche(): 
				# on regarde la possibilite de coloriage pour les  j-1 cases restantes
				return possibiliteColoriage(j-1, sequence, ligne, resultatsPrecedents)
			def caseIJnoire():  
				# il faut qu'il n y a pas de case blanche pour une longuer de Sl a partir
				# de la fin, et que la case de separation ne soit pas noire, une fois les
				# conditions satisfait, on test la  possibilite de coloriage pour le reste des cases
				conditions = all([ligne[k] != BLANCHE for k in range(j + 1 - sequence[-1], j)]) and ligne[j - sequence[-1]] != NOIRE
				return conditions and possibiliteColoriage(j-sequence[-1]-1, sequence[:-1], ligne, resultatsPrecedents)
			#optimisation
			if sum(sequence)+l-1 > j+1: 
				possibilite = False
			elif ligne[j] == BLANCHE:
				possibilite = caseIJblanche()
			elif ligne[j] == NOIRE:
				possibilite = caseIJnoire()
			elif ligne[j] == VIDE:
				possibilite = caseIJblanche() or caseIJnoire()
			else:
				possibilite = False
	resultatsPrecedents[j][l] = possibilite
	return resultatsPrecedents[j][l]


"""
	Lire le fichier en entrée , extraire la liste des colonnes et des lignes puis appeler Coloration pour 
	colorer le maximum de cases
	arguments:
		nomFichier: nom du fichier qui contient les lignes et les colonnes d'une instance
"""
def propagationPartielle(nomFichier):
	try:
		file = open(nomFichier)
	except :
		print(nomFichier+col.FAIL+col.UNDERLINE+" FILE NOT FOUND"+col.ENDC)
		exit()
	first = True
	sequencesLignes = []
	sequencesColonnes = []
	for line in file:
		sequence = line.strip().split()
		if first:
			if sequence and sequence[0] == "#":
				first = False
			else:
				sequence = [int(el) for el in sequence]
				sequencesLignes.append(sequence)
		else:
			sequence = [int(el) for el in sequence]
			sequencesColonnes.append(sequence)
	grille = -1 * \
		np.ones((len(sequencesLignes), len(sequencesColonnes)), dtype=int)
	return coloration(grille, sequencesLignes, sequencesColonnes)

"""
	Colorer le maximum de cases d'une grille
	arguments :
		grille : la grille à colorer
		sequencesLignes: la liste des séquences des lignes ou des colonnes 
		sequencesColonnes: la liste des séquences des colonnes 
"""
def coloration(grille, sequencesLignes, sequencesColonnes):
	grilleColoree = np.copy(grille)
	N, M = grilleColoree.shape[0],  grilleColoree.shape[1]
	lignesAVoir = set(range(N))
	colonnesAVoir = set(range(M))
	while len(lignesAVoir) != 0 or len(colonnesAVoir) != 0:
		for i in lignesAVoir:
			ok, grilleColoree, nouveaux = colorerLigCol(grilleColoree, i, sequencesLignes)
			if not ok:
				return IMPOSSIBLE, None
			colonnesAVoir = colonnesAVoir.union(nouveaux)
			lignesAVoir = lignesAVoir.difference(set([i]))
		for j in colonnesAVoir:
			ok, transpose, nouveaux = colorerLigCol(grilleColoree.T, j, sequencesColonnes)
			grilleColoree = transpose.T
			if not ok:
				return IMPOSSIBLE, None
			lignesAVoir = lignesAVoir.union(nouveaux)
			colonnesAVoir = colonnesAVoir.difference(set([j]))
	if np.all(grilleColoree != VIDE):
		return SUCESS, grilleColoree
	return DONT_KNOW, grilleColoree


"""
	On essaye de colorier le maximum de cases d'une ligne ou d'une seule colonne
	arguments:
		grille : la grille à colorer
		i : le numéro de la ligne ou la colonne à colorer
		sequences: la liste des séquences soit des lignes ou des colonnes 
"""
def colorerLigCol (grille, i, sequences) :
	length = len(grille[i])
	nvColonnes = set()
	ok = True
	for j in range(length) : 
		if grille[i][j] == VIDE :
			g = np.copy(grille)
			g[i][j] = NOIRE
			resultatsIntermediares = -1 * np.ones((length, len(sequences[i])+1))
			v1 = possibiliteColoriage(length-1, sequences[i], g[i], resultatsIntermediares)
			g[i][j] = BLANCHE
			resultatsIntermediares = -1 * np.ones((length, len(sequences[i])+1))
			v2 = possibiliteColoriage(length-1, sequences[i], g[i], resultatsIntermediares)
			if v1 and not v2 : 
				nvColonnes.add(j)
				grille[i][j] = NOIRE
				ok = True
			elif not v1 and v2:
				nvColonnes.add(j)
				grille[i][j] = BLANCHE
				ok = True
			elif not v1 and not v2 :
				return False, None, None
	return ok, grille, nvColonnes

